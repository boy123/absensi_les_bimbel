<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Set_jam_kantor extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Set_jam_kantor_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'set_jam_kantor/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'set_jam_kantor/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'set_jam_kantor/index.html';
            $config['first_url'] = base_url() . 'set_jam_kantor/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Set_jam_kantor_model->total_rows($q);
        $set_jam_kantor = $this->Set_jam_kantor_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'set_jam_kantor_data' => $set_jam_kantor,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'Jam Kantor',
            'konten' => 'set_jam_kantor/set_jam_kantor_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Set_jam_kantor_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'hari' => $row->hari,
		'jam_masuk' => $row->jam_masuk,
		'jam_keluar' => $row->jam_keluar,
	    );
            $this->load->view('set_jam_kantor/set_jam_kantor_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('set_jam_kantor'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'set_jam_kantor/set_jam_kantor_form',
            'konten' => 'set_jam_kantor/set_jam_kantor_form',
            'button' => 'Create',
            'action' => site_url('set_jam_kantor/create_action'),
	    'id' => set_value('id'),
	    'hari' => set_value('hari'),
	    'jam_masuk' => set_value('jam_masuk'),
	    'jam_keluar' => set_value('jam_keluar'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'hari' => $this->input->post('hari',TRUE),
		'jam_masuk' => $this->input->post('jam_masuk',TRUE),
		'jam_keluar' => $this->input->post('jam_keluar',TRUE),
	    );

            $this->Set_jam_kantor_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('set_jam_kantor'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Set_jam_kantor_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'set_jam_kantor/set_jam_kantor_form',
                'konten' => 'set_jam_kantor/set_jam_kantor_form',
                'button' => 'Update',
                'action' => site_url('set_jam_kantor/update_action'),
		'id' => set_value('id', $row->id),
		'hari' => set_value('hari', $row->hari),
		'jam_masuk' => set_value('jam_masuk', $row->jam_masuk),
		'jam_keluar' => set_value('jam_keluar', $row->jam_keluar),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('set_jam_kantor'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'hari' => $this->input->post('hari',TRUE),
		'jam_masuk' => $this->input->post('jam_masuk',TRUE),
		'jam_keluar' => $this->input->post('jam_keluar',TRUE),
	    );

            $this->Set_jam_kantor_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('set_jam_kantor'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Set_jam_kantor_model->get_by_id($id);

        if ($row) {
            $this->Set_jam_kantor_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('set_jam_kantor'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('set_jam_kantor'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('hari', 'hari', 'trim|required');
	$this->form_validation->set_rules('jam_masuk', 'jam masuk', 'trim|required');
	$this->form_validation->set_rules('jam_keluar', 'jam keluar', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Set_jam_kantor.php */
/* Location: ./application/controllers/Set_jam_kantor.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-05-14 09:06:46 */
/* https://jualkoding.com */