<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jadwal_kelas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Jadwal_kelas_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data = array(
            'konten' => 'jadwal_kelas/filter_jadwal',
            'judul_page' => 'Jadwal Kelas',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Jadwal_kelas_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_jadwal' => $row->id_jadwal,
		'tgl_jadwal' => $row->tgl_jadwal,
		'jam_awal' => $row->jam_awal,
		'jam_akhir' => $row->jam_akhir,
        'id_siswa' => $row->id_siswa,
		'id_karyawan' => $row->id_karyawan,
		'keterangan' => $row->keterangan,
	    );
            $this->load->view('jadwal_kelas/jadwal_kelas_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jadwal_kelas'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'jadwal_kelas/jadwal_kelas_form',
            'konten' => 'jadwal_kelas/jadwal_kelas_form',
            'button' => 'Create',
            'action' => site_url('jadwal_kelas/create_action'),
	    'id_jadwal' => set_value('id_jadwal'),
	    'tgl_jadwal' => set_value('tgl_jadwal'),
	    'jam_awal' => set_value('jam_awal'),
	    'jam_akhir' => set_value('jam_akhir'),
        'id_siswa' => set_value('id_siswa'),
	    'id_karyawan' => set_value('id_karyawan'),
	    'keterangan' => set_value('keterangan'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'tgl_jadwal' => $this->input->post('tgl_jadwal',TRUE),
		'jam_awal' => $this->input->post('jam_awal',TRUE),
		'jam_akhir' => $this->input->post('jam_akhir',TRUE),
        'id_siswa' => $this->input->post('id_siswa',TRUE),
		'id_karyawan' => $this->input->post('id_karyawan',TRUE),
		'keterangan' => $this->input->post('keterangan',TRUE),
	    );

            $this->Jadwal_kelas_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('jadwal_kelas?siswa='.$this->input->post('id_siswa',TRUE)));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Jadwal_kelas_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'jadwal_kelas/jadwal_kelas_form',
                'konten' => 'jadwal_kelas/jadwal_kelas_form',
                'button' => 'Update',
                'action' => site_url('jadwal_kelas/update_action'),
		'id_jadwal' => set_value('id_jadwal', $row->id_jadwal),
		'tgl_jadwal' => set_value('tgl_jadwal', $row->tgl_jadwal),
		'jam_awal' => set_value('jam_awal', $row->jam_awal),
		'jam_akhir' => set_value('jam_akhir', $row->jam_akhir),
        'id_siswa' => set_value('id_siswa', $row->id_siswa),
		'id_karyawan' => set_value('id_karyawan', $row->id_karyawan),
		'keterangan' => set_value('keterangan', $row->keterangan),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jadwal_kelas'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_jadwal', TRUE));
        } else {
            $data = array(
		'tgl_jadwal' => $this->input->post('tgl_jadwal',TRUE),
		'jam_awal' => $this->input->post('jam_awal',TRUE),
		'jam_akhir' => $this->input->post('jam_akhir',TRUE),
        'id_siswa' => $this->input->post('id_siswa',TRUE),
		'id_karyawan' => $this->input->post('id_karyawan',TRUE),
		'keterangan' => $this->input->post('keterangan',TRUE),
	    );

            $this->Jadwal_kelas_model->update($this->input->post('id_jadwal', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('jadwal_kelas?siswa='.$this->input->post('id_siswa',TRUE)));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Jadwal_kelas_model->get_by_id($id);

        if ($row) {
            $this->Jadwal_kelas_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('jadwal_kelas'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jadwal_kelas'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('tgl_jadwal', 'tgl jadwal', 'trim|required');
	$this->form_validation->set_rules('jam_awal', 'jam awal', 'trim|required');
	$this->form_validation->set_rules('jam_akhir', 'jam akhir', 'trim|required');
	$this->form_validation->set_rules('id_karyawan', 'id karyawan', 'trim|required');
	$this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');

	$this->form_validation->set_rules('id_jadwal', 'id_jadwal', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Jadwal_kelas.php */
/* Location: ./application/controllers/Jadwal_kelas.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-05-06 14:08:33 */
/* https://jualkoding.com */