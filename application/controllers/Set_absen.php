<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Set_absen extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Set_absen_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'set_absen/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'set_absen/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'set_absen/index.html';
            $config['first_url'] = base_url() . 'set_absen/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Set_absen_model->total_rows($q);
        $set_absen = $this->Set_absen_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'set_absen_data' => $set_absen,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'set_absen/set_absen_list',
            'konten' => 'set_absen/set_absen_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Set_absen_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'id_user' => $row->id_user,
		'mac_address' => $row->mac_address,
		'lock_absen' => $row->lock_absen,
	    );
            $this->load->view('set_absen/set_absen_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('set_absen'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'set_absen/set_absen_form',
            'konten' => 'set_absen/set_absen_form',
            'button' => 'Create',
            'action' => site_url('set_absen/create_action'),
	    'id' => set_value('id'),
	    'id_user' => set_value('id_user'),
	    'mac_address' => set_value('mac_address'),
	    'lock_absen' => set_value('lock_absen'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_user' => $this->input->post('id_user',TRUE),
		'mac_address' => $this->input->post('mac_address',TRUE),
		'lock_absen' => $this->input->post('lock_absen',TRUE),
	    );

            $this->Set_absen_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('set_absen'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Set_absen_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'set_absen/set_absen_form',
                'konten' => 'set_absen/set_absen_form',
                'button' => 'Update',
                'action' => site_url('set_absen/update_action'),
		'id' => set_value('id', $row->id),
		'id_user' => set_value('id_user', $row->id_user),
		'mac_address' => set_value('mac_address', $row->mac_address),
		'lock_absen' => set_value('lock_absen', $row->lock_absen),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('set_absen'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'id_user' => $this->input->post('id_user',TRUE),
		'mac_address' => $this->input->post('mac_address',TRUE),
		'lock_absen' => $this->input->post('lock_absen',TRUE),
	    );

            $this->Set_absen_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('set_absen'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Set_absen_model->get_by_id($id);

        if ($row) {
            $this->Set_absen_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('set_absen'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('set_absen'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_user', 'id user', 'trim|required');
	// $this->form_validation->set_rules('mac_address', 'mac address', 'trim|required');
	$this->form_validation->set_rules('lock_absen', 'lock absen', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Set_absen.php */
/* Location: ./application/controllers/Set_absen.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-05-09 15:45:53 */
/* https://jualkoding.com */