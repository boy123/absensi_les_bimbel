<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {

	public $image = '';
	
	public function index()
	{
        if ($this->session->userdata('level') == '') {
            redirect('login');
        }
		$data = array(
			'konten' => 'home_admin',
            'judul_page' => 'Dashboard',
		);
		$this->load->view('v_index', $data);
    }

    public function jadwal()
	{
        
		$data = array(
			'konten' => 'home_admin',
            'judul_page' => 'Dashboard',
		);
		$this->load->view('v_index', $data);
    }

    public function absen()
    {
        $data = array(
            'konten' => 'absen/absen',
            'judul_page' => 'Form Absensi',
        );
        $this->load->view('v_index', $data);
    }


    public function log_absen()
    {
        $data = array(
            'konten' => 'absen/view_absen',
            'judul_page' => 'Master Absensi',
        );
        $this->load->view('v_index', $data);
    }

    public function edit_absen()
    {
        $data = array(
            'konten' => 'absen/edit_absen',
            'judul_page' => 'Edit Absensi',
        );
        $this->load->view('v_index', $data);
    }

    public function proses_edit_absen($id_absen)
    {
        $this->db->where('id_absen', $id_absen);
        $this->db->update('log_absen', $_POST);
        $this->session->set_flashdata('message', alert_biasa('Absen berhasil diedit !','success'));
        redirect('app/absen','refresh');

    }

    public function rekap_absen()
    {
        $data = array(
            'konten' => 'absen/cari_rekap',
            'judul_page' => 'Rekap Absensi',
        );
        $this->load->view('v_index', $data);
    }

    public function komplain_siswa()
    {
        if ($_POST) {
            $simpan = $this->db->insert('komplain_siswa', $_POST);
            if ($simpan) {
                $this->session->set_flashdata('message', alert_biasa('Komplain kamu berhasil di kirim !','success'));
                redirect('app/absen','refresh');
            }
        } else {
            $data = array(
                'konten' => 'komplain_siswa/form_siswa',
                'judul_page' => 'Form Komplain',
            );
            $this->load->view('v_index', $data);
        }
    }

    public function cari_rekap()
    {
        $id_user = $this->input->post('id_user');
        $tgl1 = $this->input->post('tgl1');
        $tgl2 = $this->input->post('tgl2');

        if ($id_user == '') {
            $data['tipe'] = 'all';
            // cari id_user di log absen
            $sql = "SELECT * FROM log_absen WHERE masuk BETWEEN '$tgl1' AND '$tgl2' ";
            $data['log_absen'] = $this->db->query($sql);
        } else {
            $data['tipe'] = 'satu_orang';
            $sql = "SELECT * FROM log_absen WHERE masuk BETWEEN '$tgl1' AND '$tgl2' AND id_user='$id_user' ";
            $data['log_absen'] = $this->db->query($sql);
        }
        
        $data['tgl1'] = $tgl1;
        $data['tgl2'] = $tgl2;
        $data['user'] = get_data('a_user','id_user',$id_user,'nama_lengkap');
        $this->load->view('absen/export_absen.php',$data);
        
    }

    public function cek_mac()
    {
        $_IP_SERVER = $_SERVER['SERVER_ADDR'];
        $_IP_ADDRESS = $_SERVER['REMOTE_ADDR'];
        if($_IP_ADDRESS == $_IP_SERVER)
        {
            ob_start();
            system('ipconfig /all');
            $_PERINTAH  = ob_get_contents();
            ob_clean();
            $_PECAH = strpos($_PERINTAH, "Physical");
            $_HASIL = substr($_PERINTAH,($_PECAH+36),17);
            log_data($_PERINTAH);
            echo $_HASIL;  
        } else {
            $_PERINTAH = "arp -a $_IP_ADDRESS";
            ob_start();
            system($_PERINTAH);
            $_HASIL = ob_get_contents();
            ob_clean();
            log_r($_HASIL);
            $_PECAH = strstr($_HASIL, $_IP_ADDRESS);
            $_PECAH_STRING = explode($_IP_ADDRESS, str_replace(" ", "", $_PECAH));
            $_HASIL = substr($_PECAH_STRING[1], 0, 17);
            log_data($_HASIL);
            echo "IP Anda : ".$_IP_ADDRESS."
            MAC ADDRESS Anda : ".$_HASIL;
        }
    }

    public function absen_now()
    {

        $toleransi_masuk = get_data('set_toleransi','id',1,'toleransi_absen_awal');
        $toleransi_terlambat = get_data('set_toleransi','id',1,'toleransi_absen_telat');

        $jam_masuk_pegawai = jam_kantor_masuk();
        $jam_keluar_pegawai = jam_kantor_keluar();

        $id_user = $this->session->userdata('id_user');
        $level = $this->session->userdata('level');

        $status_kerja = '';
        if ($level == 'guru' or $level == 'pegawai') {
            $status_kerja = get_data('karyawan','id_karyawan',$this->session->userdata('keterangan'),'status_kerja');
        }

        if ($level == 'siswa' or ($level == 'guru' && $status_kerja =='part time') ) {
            if ($level == 'siswa') {
                $this->db->where('id_siswa', $id_user);
                $this->db->where('masuk_siswa', '0');
            } elseif ($level == 'guru') {
                $this->db->where('id_karyawan', $id_user);
                $this->db->where('masuk_karyawan', '0');
            }
            $this->db->order_by('jam_awal', 'asc');
            $cek_jadwal = $this->db->get('jadwal_kelas');

            if ($cek_jadwal->num_rows() > 0) {
                //cek jam saat ini apakah ada di db
                $jadwal = $cek_jadwal->row();

                $jam_masuk = $jadwal->jam_awal;
                $jam_keluar = $jadwal->jam_akhir;

                if ( strtotime(get_waktu()) < strtotime($jam_masuk) ) {

                    $selisih_time = durasi_tgl(get_waktu(),$jam_masuk);
                    if ($selisih_time > $toleransi_masuk) {
                        $lagi = $selisih_time - $toleransi_masuk;
                        $this->session->set_flashdata('message', alert_biasa('Anda belum bisa absen saat ini, silahkan tunggu '.$lagi.' menit lagi !','info'));
                        redirect('app/absen','refresh');
                    } else {
                        //bisa absen
                        $this->db->where('id_user', $id_user);
                        $this->db->where('id_jadwal', $jadwal->id_jadwal);
                        $cek_log_absen = $this->db->get('log_absen');
                        if ($cek_log_absen->num_rows() > 0) {
                            // update absen keluar
                            $this->db->where('id_user', $id_user);
                            $this->db->where('id_jadwal', $jadwal->id_jadwal);
                            $update = $this->db->update('log_absen', array('keluar'=>get_waktu()));
                            if ($update) {
                                $this->db->where('id_jadwal', $jadwal->id_jadwal);
                                if ($level == 'siswa') {
                                    $this->db->update('jadwal_kelas', array('masuk_siswa'=>'1'));
                                } else {
                                    $this->db->update('jadwal_kelas', array('masuk_karyawan'=>'1'));
                                }
                                $this->session->set_flashdata('message', alert_biasa('Terima kasih sudah absen pulang hari ini !','success'));
                                redirect('app/absen','refresh');
                            }
                        } else {
                            $simpan = $this->db->insert('log_absen', array(
                                'id_user' => $id_user,
                                'masuk' => get_waktu(),
                                'terlambat' => 0,
                                'id_jadwal' => $jadwal->id_jadwal
                            ));
                            if ($simpan) {
                                
                                $this->session->set_flashdata('message', alert_biasa('Terima kasih sudah absen hari ini !','success'));
                                redirect('app/absen','refresh');
                            }
                        }
                        

                    }

                } elseif ( strtotime(get_waktu()) > strtotime($jam_masuk) ) {
                    $selisih_time = durasi_tgl($jam_masuk,get_waktu());
                    if ($selisih_time > $toleransi_terlambat) {
                        //bisa absen
                        $terlambat = $selisih_time - $toleransi_terlambat;

                        $this->db->where('id_user', $id_user);
                        $this->db->where('id_jadwal', $jadwal->id_jadwal);
                        $cek_log_absen = $this->db->get('log_absen');
                        if ($cek_log_absen->num_rows() > 0) {
                            // update absen keluar
                            $this->db->where('id_user', $id_user);
                            $this->db->where('id_jadwal', $jadwal->id_jadwal);
                            $update = $this->db->update('log_absen', array('keluar'=>get_waktu()));
                            if ($update) {
                                $this->db->where('id_jadwal', $jadwal->id_jadwal);
                                if ($level == 'siswa') {
                                    $this->db->update('jadwal_kelas', array('masuk_siswa'=>'1'));
                                } else {
                                    $this->db->update('jadwal_kelas', array('masuk_karyawan'=>'1'));
                                }
                                $this->session->set_flashdata('message', alert_biasa('Terima kasih sudah absen pulang hari ini !','success'));
                                redirect('app/absen','refresh');
                            }
                        } else {
                            $simpan = $this->db->insert('log_absen', array(
                                'id_user' => $id_user,
                                'masuk' => get_waktu(),
                                'terlambat' => $terlambat,
                                'id_jadwal' => $jadwal->id_jadwal
                            ));
                            if ($simpan) {

                                
                                $this->session->set_flashdata('message', alert_biasa('Terima kasih sudah absen hari ini !','success'));
                                redirect('app/absen','refresh');
                            }
                        }
                        
                    } else {
                        //bisa absen
                        $this->db->where('id_user', $id_user);
                        $this->db->where('id_jadwal', $jadwal->id_jadwal);
                        $cek_log_absen = $this->db->get('log_absen');
                        if ($cek_log_absen->num_rows() > 0) {
                            // update absen keluar
                            $this->db->where('id_user', $id_user);
                            $this->db->where('id_jadwal', $jadwal->id_jadwal);
                            $update = $this->db->update('log_absen', array('keluar'=>get_waktu()));
                            if ($update) {
                                $this->db->where('id_jadwal', $jadwal->id_jadwal);
                                if ($level == 'siswa') {
                                    $this->db->update('jadwal_kelas', array('masuk_siswa'=>'1'));
                                } else {
                                    $this->db->update('jadwal_kelas', array('masuk_karyawan'=>'1'));
                                }

                                $this->session->set_flashdata('message', alert_biasa('Terima kasih sudah absen pulang hari ini !','success'));
                                redirect('app/absen','refresh');
                            }
                        } else {
                            $simpan = $this->db->insert('log_absen', array(
                                'id_user' => $id_user,
                                'masuk' => get_waktu(),
                                'terlambat' => 0,
                                'id_jadwal' => $jadwal->id_jadwal
                            ));
                            if ($simpan) {

                                
                                $this->session->set_flashdata('message', alert_biasa('Terima sudah absen hari ini !','success'));
                                redirect('app/absen','refresh');
                            }
                        }
                        
                    }
                } else {

                    $this->session->set_flashdata('message', alert_biasa('Ada kesalahan sistem !','error'));
                    redirect('app/absen','refresh');

                }


            } else {
                $this->session->set_flashdata('message', alert_biasa('Tidak ada jadwal anda hari ini, silahkan hubungi admin !','info'));
                redirect('app/absen','refresh');
            }

        
        } elseif ( $level == 'pegawai' && $status_kerja =='part time' ) {

            $this->db->where('id_user', $id_user);
            $this->db->where('status_masuk', '0');

            $this->db->order_by('jam_awal', 'asc');
            $cek_jadwal = $this->db->get('jadwal_pegawai_parttime');

            if ($cek_jadwal->num_rows() > 0) {
                //cek jam saat ini apakah ada di db
                $jadwal = $cek_jadwal->row();

                $jam_masuk = $jadwal->jam_awal;
                $jam_keluar = $jadwal->jam_akhir;

                if ( strtotime(get_waktu()) < strtotime($jam_masuk) ) {

                    $selisih_time = durasi_tgl(get_waktu(),$jam_masuk);
                    if ($selisih_time > $toleransi_masuk) {
                        $lagi = $selisih_time - $toleransi_masuk;
                        $this->session->set_flashdata('message', alert_biasa('Anda belum bisa absen saat ini, silahkan tunggu '.$lagi.' menit lagi !','info'));
                        redirect('app/absen','refresh');
                    } else {
                        //bisa absen
                        $this->db->where('id_user', $id_user);
                        $this->db->where('id_jadwal', $jadwal->id);
                        $cek_log_absen = $this->db->get('log_absen');
                        if ($cek_log_absen->num_rows() > 0) {
                            // update absen keluar
                            $this->db->where('id_user', $id_user);
                            $this->db->where('id_jadwal', $jadwal->id);
                            $update = $this->db->update('log_absen', array('keluar'=>get_waktu()));
                            if ($update) {
                                $this->db->where('id', $jadwal->id);
                                $this->db->update('jadwal_pegawai_parttime', array('status_masuk'=>'1'));
                                $this->session->set_flashdata('message', alert_biasa('Terima kasih sudah absen pulang hari ini !','success'));
                                redirect('app/absen','refresh');
                            }
                        } else {
                            $simpan = $this->db->insert('log_absen', array(
                                'id_user' => $id_user,
                                'masuk' => get_waktu(),
                                'terlambat' => 0,
                                'id_jadwal' => $jadwal->id
                            ));
                            if ($simpan) {
                                
                                $this->session->set_flashdata('message', alert_biasa('Terima kasih sudah absen hari ini !','success'));
                                redirect('app/absen','refresh');
                            }
                        }
                        

                    }

                } elseif ( strtotime(get_waktu()) > strtotime($jam_masuk) ) {
                    $selisih_time = durasi_tgl($jam_masuk,get_waktu());
                    if ($selisih_time > $toleransi_terlambat) {
                        //bisa absen
                        $terlambat = $selisih_time - $toleransi_terlambat;

                        $this->db->where('id_user', $id_user);
                        $this->db->where('id_jadwal', $jadwal->id);
                        $cek_log_absen = $this->db->get('log_absen');
                        if ($cek_log_absen->num_rows() > 0) {
                            // update absen keluar
                            $this->db->where('id_user', $id_user);
                            $this->db->where('id_jadwal', $jadwal->id);
                            $update = $this->db->update('log_absen', array('keluar'=>get_waktu()));
                            if ($update) {
                                $this->db->where('id', $jadwal->id);
                                $this->db->update('jadwal_pegawai_parttime', array('status_masuk'=>'1'));
                                $this->session->set_flashdata('message', alert_biasa('Terima kasih sudah absen pulang hari ini !','success'));
                                redirect('app/absen','refresh');
                            }
                        } else {
                            $simpan = $this->db->insert('log_absen', array(
                                'id_user' => $id_user,
                                'masuk' => get_waktu(),
                                'terlambat' => $terlambat,
                                'id_jadwal' => $jadwal->id
                            ));
                            if ($simpan) {

                                
                                $this->session->set_flashdata('message', alert_biasa('Terima kasih sudah absen hari ini !','success'));
                                redirect('app/absen','refresh');
                            }
                        }
                        
                    } else {
                        //bisa absen
                        $this->db->where('id_user', $id_user);
                        $this->db->where('id_jadwal', $jadwal->id);
                        $cek_log_absen = $this->db->get('log_absen');
                        if ($cek_log_absen->num_rows() > 0) {
                            // update absen keluar
                            $this->db->where('id_user', $id_user);
                            $this->db->where('id_jadwal', $jadwal->id);
                            $update = $this->db->update('log_absen', array('keluar'=>get_waktu()));
                            if ($update) {
                                $this->db->where('id', $jadwal->id);
                                $this->db->update('jadwal_pegawai_parttime', array('status_masuk'=>'1'));

                                $this->session->set_flashdata('message', alert_biasa('Terima kasih sudah absen pulang hari ini !','success'));
                                redirect('app/absen','refresh');
                            }
                        } else {
                            $simpan = $this->db->insert('log_absen', array(
                                'id_user' => $id_user,
                                'masuk' => get_waktu(),
                                'terlambat' => 0,
                                'id_jadwal' => $jadwal->id
                            ));
                            if ($simpan) {

                                
                                $this->session->set_flashdata('message', alert_biasa('Terima sudah absen hari ini !','success'));
                                redirect('app/absen','refresh');
                            }
                        }
                        
                    }
                } else {

                    $this->session->set_flashdata('message', alert_biasa('Ada kesalahan sistem !','error'));
                    redirect('app/absen','refresh');

                }


            } else {
                $this->session->set_flashdata('message', alert_biasa('Tidak ada jadwal anda hari ini, silahkan hubungi admin !','info'));
                redirect('app/absen','refresh');
            }


        } elseif ( ($level == 'pegawai' or $level == 'guru')  and $status_kerja == 'full time') {

            if ($jam_masuk_pegawai == 'kosong') {
                $this->session->set_flashdata('message', alert_biasa('Hari ini tidak ada jam kantor !'));
                redirect('app/absen','refresh');
            }

            $jam_masuk = date('Y-m-d '.$jam_masuk_pegawai);
            if ( strtotime(get_waktu()) < strtotime($jam_masuk) ) {
                $selisih_time = durasi_tgl(get_waktu(),$jam_masuk);
                if ($selisih_time > $toleransi_masuk) {
                    $lagi = $selisih_time - $toleransi_masuk;
                    $this->session->set_flashdata('message', alert_biasa('Anda belum bisa absen saat ini, silahkan tunggu '.$lagi.' menit lagi !','info'));
                    redirect('app/absen','refresh');
                } else {
                    //bisa absen

                    $this->db->where('id_user', $id_user);
                    $this->db->like('masuk', date('Y-m-d'), 'AFTER');
                    $cek_log_absen = $this->db->get('log_absen');
                    if ($cek_log_absen->num_rows() > 0) {
                        // update absen keluar
                        $this->db->where('id_user', $id_user);
                        $this->db->like('masuk', date('Y-m-d'), 'AFTER');
                        $update = $this->db->update('log_absen', array('keluar'=>get_waktu()));
                        if ($update) {
                            $this->session->set_flashdata('message', alert_biasa('Terima kasih sudah absen pulang hari ini !','success'));
                            redirect('app/absen','refresh');
                        }
                    } else {
                        $simpan = $this->db->insert('log_absen', array(
                            'id_user' => $id_user,
                            'masuk' => get_waktu(),
                            'terlambat' => 0,
                        ));
                        if ($simpan) {
                            $this->session->set_flashdata('message', alert_biasa('Terima kasih sudah absen hari ini !','success'));
                            redirect('app/absen','refresh');
                        }
                    }

                    
                }
            } elseif ( strtotime(get_waktu()) > strtotime($jam_masuk) ) {
                $selisih_time = durasi_tgl($jam_masuk,get_waktu());
                if ($selisih_time > $toleransi_terlambat) {
                    //bisa absen
                    $terlambat = $selisih_time - $toleransi_terlambat;

                    $this->db->where('id_user', $id_user);
                    $this->db->like('masuk', date('Y-m-d'), 'AFTER');
                    $cek_log_absen = $this->db->get('log_absen');
                    if ($cek_log_absen->num_rows() > 0) {
                        // update absen keluar
                        $this->db->where('id_user', $id_user);
                        $this->db->like('masuk', date('Y-m-d'), 'AFTER');
                        $update = $this->db->update('log_absen', array('keluar'=>get_waktu()));
                        if ($update) {
                            $this->session->set_flashdata('message', alert_biasa('Terima kasih sudah absen pulang hari ini !','success'));
                            redirect('app/absen','refresh');
                        }
                    } else {

                        $simpan = $this->db->insert('log_absen', array(
                            'id_user' => $id_user,
                            'masuk' => get_waktu(),
                            'terlambat' => $terlambat
                        ));
                        if ($simpan) {
                            $this->session->set_flashdata('message', alert_biasa('Terima kasih sudah absen hari ini !','success'));
                            redirect('app/absen','refresh');
                        }

                    }

                    
                } else {
                    //bisa absen
                    $this->db->where('id_user', $id_user);
                    $this->db->like('masuk', date('Y-m-d'), 'AFTER');
                    $cek_log_absen = $this->db->get('log_absen');
                    if ($cek_log_absen->num_rows() > 0) {
                        // update absen keluar
                        $this->db->where('id_user', $id_user);
                        $this->db->like('masuk', date('Y-m-d'), 'AFTER');
                        $update = $this->db->update('log_absen', array('keluar'=>get_waktu()));
                        if ($update) {
                            $this->session->set_flashdata('message', alert_biasa('Terima kasih sudah absen pulang hari ini !','success'));
                            redirect('app/absen','refresh');
                        }
                    } else {
                        $simpan = $this->db->insert('log_absen', array(
                            'id_user' => $id_user,
                            'masuk' => get_waktu(),
                            'terlambat' => 0,
                        ));
                        if ($simpan) {
                            $this->session->set_flashdata('message', alert_biasa('Terima kasih sudah absen hari ini !','success'));
                            redirect('app/absen','refresh');
                        }
                    }
                }
            } else {

                $this->session->set_flashdata('message', alert_biasa('Ada kesalahan sistem !','error'));
                redirect('app/absen','refresh');

            }

        } else {
            $this->session->set_flashdata('message', alert_biasa('Anda tidak bisa absen, karna tidak terdaftar !','info'));
            redirect('app/absen','refresh');
        }
    }

    public function edit_toleransi()
    {
    	if ($_POST) {
    		$this->db->where('id', 1);
    		$update = $this->db->update('set_toleransi', $_POST);
    		if ($update) {
    			$this->session->set_flashdata('message', alert_biasa('Toleransi Absen berhasil di Set','success'));
				redirect('app/set_toleransi','refresh');
    		}
    	}
    }

    public function set_toleransi()
    {
         $data = array(
            'konten' => 'set_toleransi',
            'judul_page' => 'SET TOLERANSI',
        );
        $this->load->view('v_index', $data);
    }

    public function edit_set_absen_full()
    {
        if ($_POST) {
            $this->db->where('id', 1);
            $update = $this->db->update('set_absen_full', $_POST);
            if ($update) {
                $this->session->set_flashdata('message', alert_biasa('Jam Absen berhasil di Set','success'));
                redirect('set_absen','refresh');
            }
        }
    }

    public function tes()
    {
        log_r(durasi_jam(date('Y-m-d 09:00:00'),get_waktu()));

        // $waktuawal  = date_create('2020-05-10 09:00:00'); //waktu di setting

        // $waktuakhir = date_create(); //2019-02-21 09:35 waktu sekarang

        // $diff  = date_diff($waktuawal, $waktuakhir);

         

        // echo 'Selisih waktu: ';

        // echo $diff->y . ' tahun, ';

        // echo $diff->m . ' bulan, ';

        // echo $diff->d . ' hari, ';

        // echo $diff->h . ' jam, ';

        // echo $diff->i . ' menit, ';

        // echo $diff->s . ' detik, ';
    }



   
	

	

	
}
