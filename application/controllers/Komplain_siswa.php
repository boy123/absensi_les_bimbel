<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Komplain_siswa extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Komplain_siswa_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'komplain_siswa/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'komplain_siswa/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'komplain_siswa/index.html';
            $config['first_url'] = base_url() . 'komplain_siswa/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Komplain_siswa_model->total_rows($q);
        $komplain_siswa = $this->Komplain_siswa_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'komplain_siswa_data' => $komplain_siswa,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'komplain_siswa/komplain_siswa_list',
            'konten' => 'komplain_siswa/komplain_siswa_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Komplain_siswa_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_komplain' => $row->id_komplain,
		'id_user' => $row->id_user,
		'komplain' => $row->komplain,
	    );
            $this->load->view('komplain_siswa/komplain_siswa_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('komplain_siswa'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'komplain_siswa/komplain_siswa_form',
            'konten' => 'komplain_siswa/komplain_siswa_form',
            'button' => 'Create',
            'action' => site_url('komplain_siswa/create_action'),
	    'id_komplain' => set_value('id_komplain'),
	    'id_user' => set_value('id_user'),
	    'komplain' => set_value('komplain'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_user' => $this->input->post('id_user',TRUE),
		'komplain' => $this->input->post('komplain',TRUE),
	    );

            $this->Komplain_siswa_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('komplain_siswa'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Komplain_siswa_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'komplain_siswa/komplain_siswa_form',
                'konten' => 'komplain_siswa/komplain_siswa_form',
                'button' => 'Update',
                'action' => site_url('komplain_siswa/update_action'),
		'id_komplain' => set_value('id_komplain', $row->id_komplain),
		'id_user' => set_value('id_user', $row->id_user),
		'komplain' => set_value('komplain', $row->komplain),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('komplain_siswa'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_komplain', TRUE));
        } else {
            $data = array(
		'id_user' => $this->input->post('id_user',TRUE),
		'komplain' => $this->input->post('komplain',TRUE),
	    );

            $this->Komplain_siswa_model->update($this->input->post('id_komplain', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('komplain_siswa'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Komplain_siswa_model->get_by_id($id);

        if ($row) {
            $this->Komplain_siswa_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('komplain_siswa'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('komplain_siswa'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_user', 'id user', 'trim|required');
	$this->form_validation->set_rules('komplain', 'komplain', 'trim|required');

	$this->form_validation->set_rules('id_komplain', 'id_komplain', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Komplain_siswa.php */
/* Location: ./application/controllers/Komplain_siswa.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-05-10 08:15:53 */
/* https://jualkoding.com */