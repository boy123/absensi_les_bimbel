<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jadwal_pegawai_parttime extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Jadwal_pegawai_parttime_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'jadwal_pegawai_parttime/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'jadwal_pegawai_parttime/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'jadwal_pegawai_parttime/index.html';
            $config['first_url'] = base_url() . 'jadwal_pegawai_parttime/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Jadwal_pegawai_parttime_model->total_rows($q);
        $jadwal_pegawai_parttime = $this->Jadwal_pegawai_parttime_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'jadwal_pegawai_parttime_data' => $jadwal_pegawai_parttime,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'Pegawai Part Time',
            'konten' => 'jadwal_pegawai_parttime/jadwal_pegawai_parttime_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Jadwal_pegawai_parttime_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'tgl_jadwal' => $row->tgl_jadwal,
		'id_user' => $row->id_user,
		'jam_awal' => $row->jam_awal,
		'jam_akhir' => $row->jam_akhir,
		'status_masuk' => $row->status_masuk,
	    );
            $this->load->view('jadwal_pegawai_parttime/jadwal_pegawai_parttime_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jadwal_pegawai_parttime'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'jadwal_pegawai_parttime/jadwal_pegawai_parttime_form',
            'konten' => 'jadwal_pegawai_parttime/jadwal_pegawai_parttime_form',
            'button' => 'Create',
            'action' => site_url('jadwal_pegawai_parttime/create_action'),
	    'id' => set_value('id'),
	    'tgl_jadwal' => set_value('tgl_jadwal'),
	    'id_user' => set_value('id_user'),
	    'jam_awal' => set_value('jam_awal'),
	    'jam_akhir' => set_value('jam_akhir'),
	    'status_masuk' => set_value('status_masuk'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'tgl_jadwal' => $this->input->post('tgl_jadwal',TRUE),
		'id_user' => $this->input->post('id_user',TRUE),
		'jam_awal' => $this->input->post('jam_awal',TRUE),
		'jam_akhir' => $this->input->post('jam_akhir',TRUE),
		'status_masuk' => $this->input->post('status_masuk',TRUE),
	    );

            $this->Jadwal_pegawai_parttime_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('jadwal_pegawai_parttime'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Jadwal_pegawai_parttime_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'jadwal_pegawai_parttime/jadwal_pegawai_parttime_form',
                'konten' => 'jadwal_pegawai_parttime/jadwal_pegawai_parttime_form',
                'button' => 'Update',
                'action' => site_url('jadwal_pegawai_parttime/update_action'),
		'id' => set_value('id', $row->id),
		'tgl_jadwal' => set_value('tgl_jadwal', $row->tgl_jadwal),
		'id_user' => set_value('id_user', $row->id_user),
		'jam_awal' => set_value('jam_awal', $row->jam_awal),
		'jam_akhir' => set_value('jam_akhir', $row->jam_akhir),
		'status_masuk' => set_value('status_masuk', $row->status_masuk),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jadwal_pegawai_parttime'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'tgl_jadwal' => $this->input->post('tgl_jadwal',TRUE),
		'id_user' => $this->input->post('id_user',TRUE),
		'jam_awal' => $this->input->post('jam_awal',TRUE),
		'jam_akhir' => $this->input->post('jam_akhir',TRUE),
		'status_masuk' => $this->input->post('status_masuk',TRUE),
	    );

            $this->Jadwal_pegawai_parttime_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('jadwal_pegawai_parttime'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Jadwal_pegawai_parttime_model->get_by_id($id);

        if ($row) {
            $this->Jadwal_pegawai_parttime_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('jadwal_pegawai_parttime'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jadwal_pegawai_parttime'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('tgl_jadwal', 'tgl jadwal', 'trim|required');
	$this->form_validation->set_rules('id_user', 'id user', 'trim|required');
	$this->form_validation->set_rules('jam_awal', 'jam awal', 'trim|required');
	$this->form_validation->set_rules('jam_akhir', 'jam akhir', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Jadwal_pegawai_parttime.php */
/* Location: ./application/controllers/Jadwal_pegawai_parttime.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-05-14 09:06:38 */
/* https://jualkoding.com */