<div class="panel panel-success">
  <div class="panel-heading">Cari Jadwal</div>
  <div class="panel-body">
  	<form action="" method="GET">
		<div class="form-group">
			<label>Cari Siswa</label>
			<select name="siswa" class="form-control select2">
				<option value="">Pilih Siswa</option>
				<?php 
				$this->db->where('level', 'siswa');
				foreach ($this->db->get('a_user')->result() as $rw): ?>
					<option value="<?php echo $rw->id_user ?>"><?php echo $rw->nama_lengkap ?></option>
				<?php endforeach ?>
			</select>
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-primary">Kirim</button>
		</div>
	</form>
  </div>
</div>

<?php if ($_GET): ?>
	<div class="panel panel-success">
	  <div class="panel-heading">List Jadwal</div>
	  <div class="panel-body">
	  	<p>
	  		<a href="jadwal_kelas/create/<?php echo $this->input->get('siswa') ?>" class="btn btn-primary">Tambah Jadwal</a>
	  	</p>

	  	<div class="table-responsive">
	  		<table class="table table-striped" id="example1">
	  			<thead>
	  				<tr>
	  					<th>No.</th>
	  					<th>Tanggal</th>
	  					<th>Hari</th>
	  					<th>Jam</th>
	  					<th>Nama Siswa</th>
	  					<th>Nama Guru</th>
	  					<th>Keterangan</th>
	  					<th>Pilihan</th>
	  				</tr>
	  			</thead>
	  			<tbody>
	  				<?php 
	  				$no = 1;
	  				$id_user = $this->input->get('siswa');
	  				$this->db->where('id_siswa', $id_user);
	  				foreach ($this->db->get('jadwal_kelas')->result() as $rw) {
	  				 ?>
	  				<tr>
	  					<td><?php echo $no; ?></td>
	  					<td><?php echo $rw->tgl_jadwal; ?></td>
	  					<td><?php echo hari_id($rw->tgl_jadwal); ?></td>
	  					<td><?php echo $rw->jam_awal.' - '.$rw->jam_akhir; ?></td>
	  					<td><?php echo get_data('a_user','id_user',$rw->id_siswa,'nama_lengkap'); ?></td>
	  					<td><?php echo get_data('a_user','id_user',$rw->id_karyawan,'nama_lengkap'); ?></td>
	  					<td><?php echo $rw->keterangan ?></td>
	  					<td>
	  						<a href="jadwal_kelas/update/<?php echo $rw->id_jadwal ?>" class="label label-info">Edit</a>
	  						<a href="jadwal_kelas/delete/<?php echo $rw->id_jadwal ?>" class="label label-danger">Hapus</a>
	  					</td>
	  				</tr>
	  				<?php } ?>
	  			</tbody>
	  		</table>
	  	</div>
	  </div>
	</div>
<?php endif ?>