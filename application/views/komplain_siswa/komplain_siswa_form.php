
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Id User <?php echo form_error('id_user') ?></label>
            <input type="text" class="form-control" name="id_user" id="id_user" placeholder="Id User" value="<?php echo $id_user; ?>" />
        </div>
	    <div class="form-group">
            <label for="komplain">Komplain <?php echo form_error('komplain') ?></label>
            <textarea class="form-control" rows="3" name="komplain" id="komplain" placeholder="Komplain"><?php echo $komplain; ?></textarea>
        </div>
	    <input type="hidden" name="id_komplain" value="<?php echo $id_komplain; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('komplain_siswa') ?>" class="btn btn-default">Cancel</a>
	</form>
   