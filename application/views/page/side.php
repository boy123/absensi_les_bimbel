<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="image/user/<?php echo $this->session->userdata('foto'); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('nama'); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        
        <?php if ($this->session->userdata('level') == 'admin') { ?>
        <li><a href="app"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        
        <li><a href="siswa"><i class="fa fa-clone"></i> <span>Data Siswa</span></a></li>
        <li><a href="karyawan"><i class="fa fa-clone"></i> <span>Data Karyawan</span></a></li>
        <li><a href="jadwal_kelas"><i class="fa fa-cloud-upload"></i> <span>Master Jadwal</span></a></li>
        
        

        <li class="treeview" style="height: auto;">
          <a href="#">
            <i class="fa fa fa-cogs"></i>
            <span>Setting Absen</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="display: none;">
            <li><a href="set_absen"><i class="fa fa-circle-o"></i> Set Lock Absen</a></li>
            <li><a href="app/set_toleransi"><i class="fa fa-circle-o"></i> Set Toleransi</a></li>
            <li><a href="set_jam_kantor"><i class="fa fa-circle-o"></i> Jam Kantor</a></li>
            <li><a href="jadwal_pegawai_parttime"><i class="fa fa-circle-o"></i> Jadwal Pegawai Part Time</a></li>
            
          </ul>
        </li>

        <li><a href="app/log_absen"><i class="fa fa-calendar-plus-o"></i> <span>Master Absen</span></a></li>
        <li><a href="komplain_siswa"><i class="fa fa-send"></i> <span>Komplain Siswa</span></a></li>
        
        
        <li><a href="app/rekap_absen"><i class="fa fa-flag-checkered"></i> <span>Rekap Absen</span></a></li>
        
        <li><a href="a_user"><i class="fa fa-users"></i> <span>Manajemen User</span></a></li>
      
        <?php } elseif ($this->session->userdata('level')=='siswa') { ?>
        <li><a href="app/absen"><i class="fa fa-check-square-o"></i> <span>Absensi</span></a></li>
        <li><a href="app/komplain_siswa"><i class="fa fa-hand-stop-o"></i> <span>Komplain Siswa</span></a></li>

        <?php } elseif ($this->session->userdata('level')=='guru' or $this->session->userdata('level')=='pegawai' ) { ?>
        <li><a href="app/absen"><i class="fa fa-check-square-o"></i> <span>Absensi</span></a></li>



        <?php } ?>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Faqs</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Tentang Aplikasi</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>