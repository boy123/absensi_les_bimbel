
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="date">Tgl Jadwal <?php echo form_error('tgl_jadwal') ?></label>
            <input type="date" class="form-control" name="tgl_jadwal" id="tgl_jadwal" placeholder="Tgl Jadwal" value="<?php echo $tgl_jadwal; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Pegawai <?php echo form_error('id_user') ?></label>
            <!-- <input type="text" class="form-control" name="id_user" id="id_user" placeholder="Id User" value="<?php echo $id_user; ?>" /> -->
            <select name="id_user" class="form-control">
                <option value="<?php echo $id_user ?>"><?php echo $id_user ?></option>
                <?php
                $sql = "SELECT a.id_user, a.nama_lengkap FROM a_user as a, karyawan as k WHERE a.keterangan = k.id_karyawan and k.status_karyawan='pegawai' and k.status_kerja='part time' ";
                 foreach ($this->db->query($sql)->result() as $key => $value): ?>
                    <option value="<?php echo $value->id_user ?>"><?php echo $value->nama_lengkap ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="time">Jam Awal <?php echo form_error('jam_awal') ?></label>
            <input type="time" class="form-control" name="jam_awal" id="jam_awal" placeholder="Jam Awal" value="<?php echo $jam_awal; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Jam Akhir <?php echo form_error('jam_akhir') ?></label>
            <input type="time" class="form-control" name="jam_akhir" id="jam_akhir" placeholder="Jam Akhir" value="<?php echo $jam_akhir; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('jadwal_pegawai_parttime') ?>" class="btn btn-default">Cancel</a>
	</form>
   