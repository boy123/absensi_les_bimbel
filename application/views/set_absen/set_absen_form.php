
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">User <?php echo form_error('id_user') ?></label>
            <!-- <input type="text" class="form-control" name="id_user" id="id_user" placeholder="Id User" value="<?php echo $id_user; ?>" /> -->
            <select name="id_user" class="form-control">
                <option value="<?php echo $id_user ?>"><?php echo $id_user ?></option>
                <?php 
                $this->db->where('level', 'pegawai');
                $this->db->or_where('level', 'guru');
                foreach ($this->db->get('a_user')->result() as $key => $value): ?>
                    <option value="<?php echo $value->id_user ?>"><?php echo $value->nama_lengkap ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="varchar">Mac Address <?php echo form_error('mac_address') ?></label>
            <input type="text" class="form-control" name="mac_address" id="mac_address" placeholder="Mac Address" value="<?php echo $mac_address; ?>" />
        </div>
	    <div class="form-group">
            <label for="enum">Lock Absen <?php echo form_error('lock_absen') ?></label>
            <!-- <input type="text" class="form-control" name="lock_absen" id="lock_absen" placeholder="Lock Absen" value="<?php echo $lock_absen; ?>" /> -->
            <select name="lock_absen" class="form-control">
                <option value="0">Tidak</option>
                <option value="1">Ya</option>
            </select>
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('set_absen') ?>" class="btn btn-default">Cancel</a>
	</form>
   