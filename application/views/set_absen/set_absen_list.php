

<div class="panel panel-success">
  <div class="panel-heading">Lock Absen Karyawan</div>
  <div class="panel-body">
      <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('set_absen/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('set_absen/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('set_absen'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
        <th>User</th>
        <th>Mac Address</th>
        <th>Lock Absen</th>
        <th>Action</th>
            </tr><?php
            foreach ($set_absen_data as $set_absen)
            {
                ?>
                <tr>
            <td width="80px"><?php echo ++$start ?></td>
            <td><?php echo get_data('a_user','id_user',$set_absen->id_user,'nama_lengkap') ?></td>
            <td><?php echo $set_absen->mac_address ?></td>
            <td><?php echo $retVal = ($set_absen->lock_absen == '0') ? 'Tidak' : 'Ya' ; ?></td>
            <td style="text-align:center" width="200px">
                <?php 
                echo anchor(site_url('set_absen/update/'.$set_absen->id),'<span class="label label-info">Ubah</span>'); 
                echo ' | '; 
                echo anchor(site_url('set_absen/delete/'.$set_absen->id),'<span class="label label-danger">Hapus</span>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                ?>
            </td>
        </tr>
                <?php
            }
            ?>
        </table>
        </div>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
        </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
  </div>
</div>
        
    