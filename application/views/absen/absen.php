<div class="row">
	<div class="col-md-6">
		<div class="panel panel-success">
		  <div class="panel-heading">Absensi</div>
		  <div class="panel-body">
		  	<center>
				<h2>Jam Saat Ini</h2>
				<h1 id="waktu"></h1>
				<p>
					<a href="app/absen_now" class="btn btn-info">ABSEN SEKARANG</a>
				</p>
			</center>
		  </div>
		</div>
		
	</div>

	<div class="col-md-6">
		<div class="panel panel-success">
		  <div class="panel-heading">Log Absensi</div>
		  <div class="panel-body">
		  	<table class="table table-striped">
		  		<tr>
		  			<td>Hari</td>
		  			<td>Masuk</td>
		  			<td>Pulang</td>
		  			<td>Terlambat</td>
		  		</tr>

		  		<?php 
		  		$this->db->where('id_user', $this->session->userdata('id_user'));
		  		$this->db->limit(5);
		  		foreach ($this->db->get('log_absen')->result() as $key => $value): ?>
		  		<tr>
		  			<td><?php echo hari_id(substr($value->masuk, 0, 10)) ?></td>
		  			<td><?php echo $value->masuk ?></td>
		  			<td><?php echo $value->keluar ?></td>
		  			<td><?php echo $value->terlambat ?> Menit</td>
		  		</tr>
		  		<?php endforeach ?>
		  	</table>
		  </div>
		</div>
		
	</div>

</div>

<script type="text/javascript">
	
		setInterval(function() {
			var dt = new Date();
			var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
			$('#waktu').html(time);
		},1000);

</script>