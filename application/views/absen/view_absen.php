<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Absen Siswa</a></li>
    <li><a data-toggle="tab" href="#menu1">Absen Guru</a></li>
    <li><a data-toggle="tab" href="#menu2">Absen Pagawai</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">

      <div style="padding: 20px;"></div>

      <table class="table table-striped" id="example1">
        <thead>
          <tr>
            <th>Hari</th>
            <th>Nama</th>
            <th>Masuk</th>
            <th>Keluar</th>
            <th>Terlambat</th>
            <th>Option</th>
          </tr>
        </thead>
        <tbody>
        <?php 
        $tab = 'siswa';
        $sql = "SELECT * FROM log_absen WHERE id_user IN(SELECT id_user FROM a_user WHERE level='siswa') ";
        foreach ($this->db->query($sql)->result() as $value) {
         ?>
          <tr>
            <td><?php echo hari_id(substr($value->masuk, 0, 10)) ?></td>
            <td><?php echo get_data('a_user','id_user',$value->id_user,'nama_lengkap') ?></td>
            <td><?php echo $value->masuk ?></td>
            <td><?php echo $value->keluar ?></td>
            <td><?php echo $value->terlambat ?> Menit</td>
            <td>
              <a href="#" class="label label-info" data-toggle="modal" data-target="#mJadwal_<?php echo $value->id_absen ?>">lihat jadwal</a>
              <a href="app/edit_absen/<?php echo $value->id_absen ?>" class="label label-primary" >Edit Absen </a>


              <!-- Modal -->
              <div id="mJadwal_<?php echo $value->id_absen ?>" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Detail Jadwal</h4>
                    </div>
                    <div class="modal-body">
                      <?php 
                      $d = $this->db->get_where('jadwal_kelas', array('id_jadwal'=>$value->id_jadwal))->row();
                       ?>
                      <table class="table table-striped">
                        <tr>
                          <td>Tanggal</td>
                          <td>: <?php echo $d->tgl_jadwal ?></td>
                        </tr>

                        <tr>
                          <td>Dari</td>
                          <td>: <?php echo $d->jam_awal ?></td>
                        </tr>
                        <tr>
                          <td>Sampai</td>
                          <td>: <?php echo $d->jam_akhir ?></td>
                        </tr>
                        <tr>
                          <td>Pengajar</td>
                          <td>: <?php echo get_data('a_user','id_user',$d->id_karyawan,'nama_lengkap') ?></td>
                        </tr>
                        <tr>
                          <td>Siswa</td>
                          <td>: <?php echo get_data('a_user','id_user',$d->id_siswa,'nama_lengkap') ?></td>
                        </tr>
                        <tr>
                          <td>Keterangan</td>
                          <td>: <?php echo $d->keterangan ?></td>
                        </tr>
                      </table>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>

                </div>
              </div>

              



            </td>
          </tr>
        <?php } ?>
        </tbody>
      </table>
    </div>

    <div id="menu1" class="tab-pane fade">
      
      <div style="padding: 20px;"></div>

      <table class="table table-striped" id="example2">
        <thead>
          <tr>
            <th>Hari</th>
            <th>Nama</th>
            <th>Masuk</th>
            <th>Keluar</th>
            <th>Terlambat</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
        <?php 
        $tab = 'guru';
        $sql = "SELECT * FROM log_absen WHERE id_user IN(SELECT id_user FROM a_user WHERE level='guru') ";
        foreach ($this->db->query($sql)->result() as $value) {
         ?>
          <tr>
            <td><?php echo hari_id(substr($value->masuk, 0, 10)) ?></td>
            <td><?php echo get_data('a_user','id_user',$value->id_user,'nama_lengkap') ?></td>
            <td><?php echo $value->masuk ?></td>
            <td><?php echo $value->keluar ?></td>
            <td><?php echo $value->terlambat ?> Menit</td>
            <td>

              <?php
              $id_karyawan = get_data('a_user','id_user',$value->id_user,'keterangan');
              $status_kerja = '';
              if ($id_karyawan != '') {
                $status_kerja = get_data('karyawan','id_karyawan',$id_karyawan,'status_kerja');
              }
              echo $retVal = ($status_kerja == 'full time') ? '<span class="label label-info">'.$status_kerja.'</span>' : '<span class="label label-warning">'.$status_kerja.'</span>' ;
               ?>
              
              <?php if ($status_kerja=='part time'): ?>
                
              
              <a href="#" class="label label-success" data-toggle="modal" data-target="#mJadwal_<?php echo $value->id_absen ?>">lihat jadwal</a>
              <a href="app/edit_absen/<?php echo $value->id_absen ?>" class="label label-primary" >Edit Absen </a>


              <!-- Modal -->
              <div id="mJadwal_<?php echo $value->id_absen ?>" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Detail Jadwal</h4>
                    </div>
                    <div class="modal-body">
                      <?php 
                      $d = $this->db->get_where('jadwal_kelas', array('id_jadwal'=>$value->id_jadwal))->row();
                       ?>
                      <table class="table table-striped">
                        <tr>
                          <td>Tanggal</td>
                          <td>: <?php echo $d->tgl_jadwal ?></td>
                        </tr>

                        <tr>
                          <td>Dari</td>
                          <td>: <?php echo $d->jam_awal ?></td>
                        </tr>
                        <tr>
                          <td>Sampai</td>
                          <td>: <?php echo $d->jam_akhir ?></td>
                        </tr>
                        <tr>
                          <td>Pengajar</td>
                          <td>: <?php echo get_data('a_user','id_user',$d->id_karyawan,'nama_lengkap') ?></td>
                        </tr>
                        <tr>
                          <td>Siswa</td>
                          <td>: <?php echo get_data('a_user','id_user',$d->id_siswa,'nama_lengkap') ?></td>
                        </tr>
                        <tr>
                          <td>Keterangan</td>
                          <td>: <?php echo $d->keterangan ?></td>
                        </tr>
                      </table>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>

                </div>
              </div>

              <?php endif ?>



            </td>
          </tr>
        <?php } ?>
        </tbody>
      </table>

    </div>

    <div id="menu2" class="tab-pane fade">
      
      <div style="padding: 20px;"></div>

      <table class="table table-striped" id="example3">
        <thead>
          <tr>
            <th>Hari</th>
            <th>Nama</th>
            <th>Masuk</th>
            <th>Keluar</th>
            <th>Terlambat</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
        <?php 
        $tab = 'pegawai';
        $sql = "SELECT * FROM log_absen WHERE id_user IN(SELECT id_user FROM a_user WHERE level='pegawai') ";
        foreach ($this->db->query($sql)->result() as $value) {
         ?>
          <tr>
            <td><?php echo hari_id(substr($value->masuk, 0, 10)) ?></td>
            <td><?php echo get_data('a_user','id_user',$value->id_user,'nama_lengkap') ?></td>
            <td><?php echo $value->masuk ?></td>
            <td><?php echo $value->keluar ?></td>
            <td><?php echo $value->terlambat ?> Menit</td>
            <td>

               <?php
                $id_karyawan = get_data('a_user','id_user',$value->id_user,'keterangan');
                $status_kerja = '';
                if ($id_karyawan != '') {
                  $status_kerja = get_data('karyawan','id_karyawan',$id_karyawan,'status_kerja');
                }
                echo $retVal = ($status_kerja == 'full time') ? '<span class="label label-info">'.$status_kerja.'</span>' : '<span class="label label-warning">'.$status_kerja.'</span>' ;
                 ?>

                <a href="app/edit_absen/<?php echo $value->id_absen ?>" class="label label-primary" >Edit Absen </a>
            </td>
          </tr>
        <?php } ?>
        </tbody>
      </table>

    </div>
    
  </div>