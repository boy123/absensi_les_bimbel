<?php 
$id_absen  = $this->uri->segment(3);
$rw = $this->db->get_where('log_absen', array('id_absen'=>$id_absen))->row();
 ?>

<form action="app/edit_absen/<?php echo $id_absen ?>" method="POST">
	<div class="form-group">
		<label>Nama</label>
		<input type="text" class="form-control" value="<?php echo get_data('a_user','id_user',$rw->id_user,'nama_lengkap') ?>" readonly>
	</div>
	<div class="form-group">
		<label>Absen Masuk</label>
		<input type="text" name="masuk" class="form-control" value="<?php echo $rw->masuk ?>">
	</div>
	<div class="form-group">
		<label>Absen Keluar</label>
		<input type="text" name="keluar" class="form-control" value="<?php echo $rw->keluar ?>">
	</div>
	<div class="form-group">
		<label>Terlambat</label>
		<br>
		<div class="row">
			<div class="col-md-3">
				<input type="text" name="terlambat" class="form-control" value="<?php echo $rw->terlambat ?>">
			</div>
			<div class="col-md-3">
				 Menit
			</div>
		</div>
		
	</div>
	<!-- <div class="form-group">
		<label>Status</label>
		<select name="status" class="form-control" >
			<option value="">Pilih Status Absen</option>
			<option value="alfa">alfa</option>
			<option value="izin">izin</option>
			<option value="sakit">sakit</option>
			<option value="dinas">dinas</option>
		</select>
	</div>
	<div class="form-group">
		<label>Keterangan Tambahan</label>
		<textarea name="keterangan" class="form-control" placeholder="Keterangan Tambahan "></textarea>
	</div> -->
	<div class="form-group">
		<button type="submit" class="btn btn-primary">Ubah</button>
		<a href="app/log_absen" class="btn btn-warning">Kembali</a>
	</div>
</form>