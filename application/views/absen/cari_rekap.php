<div class="panel panel-success">
  <div class="panel-heading">Rekap Absensi</div>
  <div class="panel-body">
  	<form action="app/cari_rekap" method="POST" target="_blank">
  		<div class="form-group">
  			<label>Nama</label>
  			<select name="id_user" class="form-control select2">
  				<option value="">--Semua User--</option>
  				<?php 
          $sql = $this->db->query("SELECT * FROM `a_user`
            WHERE `level` IN('siswa','guru','pegawai')");
          // log_r($this->db->last_query());
  				foreach ($sql->result() as $rw): ?>
  					<option value="<?php echo $rw->id_user ?>"><?php echo $rw->nama_lengkap ?></option>
  				<?php endforeach ?>
  			</select>
  		</div>

      <div class="form-group">
        <label>Dari</label>
        <input type="date" name="tgl1" class="form-control" required="">
      </div>

      <div class="form-group">
        <label>Sampai</label>
        <input type="date" name="tgl2" class="form-control" required="">
      </div>

      <div class="form-group">
        <button type="submit" class="btn btn-info">Export</button>
      </div>

  	</form>
  </div>
</div>