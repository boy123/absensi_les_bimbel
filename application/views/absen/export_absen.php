<?php 
if ($tipe=='satu_orang') {
	$nama_file = "Rekap Absen $user";
} else {
	$nama_file = "Rekap Absen Semua";
}
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Rekap .xls");

 ?>

<center><h3>Rekap Absen</h3></center>
<?php

if ($tipe == 'satu_orang') {
	?>
	
	<table>
		<tr>
			<td>Nama</td>
			<td>:</td>
			<td><?php echo $user ?></td>
		</tr>
		<tr>
			<td>Dari </td>
			<td>:</td>
			<td><?php echo $tgl1 ?></td>
		</tr>
		<tr>
			<td>Sampai </td>
			<td>:</td>
			<td><?php echo $tgl2 ?></td>
		</tr>
	</table>


	<?php
} else {
 ?>

<table>
		<tr>
			<td>Dari </td>
			<td>:</td>
			<td><?php echo $tgl1 ?></td>
		</tr>
		<tr>
			<td>Sampai </td>
			<td>:</td>
			<td><?php echo $tgl2 ?></td>
		</tr>
	</table>

<?php } ?>

<table border="1">
	<tr>
		<td>No.</td>
		<td>Nama</td>
		<td>Status</td>
		<td>Absen Masuk</td>
		<td>Absen Keluar</td>
		<td>Terlambat</td>
	</tr>

	<?php 
	$no = 1;
	foreach ($log_absen->result() as $rw): ?>
		<tr>
			<td><?php echo $no ?></td>
			<td><?php echo get_data('a_user','id_user',$rw->id_user,'nama_lengkap') ?></td>
			<td>
				<?php
				$id_karyawan = get_data('a_user','id_user',$rw->id_user,'keterangan');
				$status_kerja = '';
				if ($id_karyawan != '') {
					$status_kerja = get_data('karyawan','id_karyawan',$id_karyawan,'status_kerja');
				}
				echo $status_kerja;
				 ?>

			</td>
			<td><?php echo $rw->masuk ?></td>
			<td><?php echo $rw->keluar ?></td>
			<td><?php echo $rw->terlambat.' menit' ?></td>
		</tr>
	<?php $no++; endforeach ?>

</table>