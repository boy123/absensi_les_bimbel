
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Hari <?php echo form_error('hari') ?></label>
            <!-- <input type="text" class="form-control" name="hari" id="hari" placeholder="Hari" value="<?php echo $hari; ?>" /> -->
            <select name="hari" class="form-control">
                <option value="<?php echo $hari ?>"><?php echo $hari ?></option>
                <?php foreach ($this->db->get('hari')->result() as $key => $value): ?>
                    <option value="<?php echo $value->hari ?>"><?php echo $value->hari ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="time">Jam Masuk <?php echo form_error('jam_masuk') ?></label>
            <input type="time" class="form-control" name="jam_masuk" id="jam_masuk" placeholder="Jam Masuk" value="<?php echo $jam_masuk; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Jam Keluar <?php echo form_error('jam_keluar') ?></label>
            <input type="time" class="form-control" name="jam_keluar" id="jam_keluar" placeholder="Jam Keluar" value="<?php echo $jam_keluar; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('set_jam_kantor') ?>" class="btn btn-default">Cancel</a>
	</form>
   