<?php 
if ($id_karyawan == '') {
    $id_karyawan = $this->uri->segment(3);
}
 ?>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="date">Tanggal Jadwal <?php echo form_error('tgl_jadwal') ?></label>
            <input type="date" class="form-control" name="tgl_jadwal" id="tgl_jadwal" placeholder="Tgl Jadwal" value="<?php echo $tgl_jadwal; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Jam Awal <?php echo form_error('jam_awal') ?></label>
            <input type="time" class="form-control" name="jam_awal" id="jam_awal" placeholder="Jam Awal" value="<?php echo $jam_awal; ?>" />
        </div>
	    <div class="form-group">
            <label for="time">Jam Akhir <?php echo form_error('jam_akhir') ?></label>
            <input type="time" class="form-control" name="jam_akhir" id="jam_akhir" placeholder="Jam Akhir" value="<?php echo $jam_akhir; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Karyawan <?php echo form_error('id_karyawan') ?></label>
            <!-- <input type="text" class="form-control" name="id_karyawan" id="id_karyawan" placeholder="Id Karyawan" value="<?php echo get_data('karyawan','id_karyawan',$id_karyawan,'nama'); ?>" /> -->
            <select name="id_karyawan" class="form-control">
                <option value="<?php echo $id_karyawan ?>"><?php echo get_data('karyawan','id_karyawan',$id_karyawan,'nama'); ?></option>
            </select>
        </div>
	    <div class="form-group">
            <label for="keterangan">Keterangan <?php echo form_error('keterangan') ?></label>
            <textarea class="form-control" rows="3" name="keterangan" id="keterangan" placeholder="Keterangan"><?php echo $keterangan; ?></textarea>
        </div>
	    <input type="hidden" name="id_jadwal" value="<?php echo $id_jadwal; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('jadwal_kelas') ?>" class="btn btn-default">Cancel</a>
	</form>
   