<div class="row">
  <div class="col-md-6">
    <div class="panel panel-success">
      <div class="panel-heading">SET Toleransi</div>
      <div class="panel-body">
          <form action="app/edit_toleransi" method="POST">
              <div class="form-group">
                  <label>Toleransi Absen lebih awal</label>
                  <input type="text" class="form-control" name="toleransi_absen_awal" value="<?php echo get_data('set_toleransi','id',1,'toleransi_absen_awal') ?>">
              </div>
              <div class="form-group">
                  <label>Toleransi Absen Terlambat</label>
                  <input type="text" class="form-control" name="toleransi_absen_telat" value="<?php echo get_data('set_toleransi','id',1,'toleransi_absen_telat') ?>">
              </div>
              <div class="form-group">
                  <button type="submit" class="btn btn-info">Simpan</button>
              </div>
          </form>
      </div>
    </div>
  </div>


  

  
</div>